# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Checks that the machine has access to the Internet.
"""

from time import sleep
from urllib import urlopen
from datetime import timedelta

from customLogging import logger


def check_internet_connection(sample_page="http://www.google.com",
			      sleep_time=timedelta(minutes=1)):
	while 1:
		try:
			urlopen(sample_page)
			break
		except:
			logger.debug("Couldn't connect to internet. Sleeping...")
			sleep(sleep_time.total_seconds())