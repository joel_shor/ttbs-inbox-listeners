# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Creates a logger that prints to the screen at DEBUG, to file on INFO,
and email on ERROR.
"""

import logging.handlers as log_h
import logging
from os.path import join

from constants import pwds, exc_pm


# Send email for errors / exceptions
smtp_h = log_h.SMTPHandler(exc_pm["smtp"], exc_pm["from"],
			   exc_pm["to"], "TTBS Error",
			   (exc_pm["from"],pwds[exc_pm["from"]]), ())
smtp_h.setLevel("ERROR")

# Write to file for important events
file_h = logging.FileHandler(join(exc_pm["folder"], "TTBS Logfile.txt"), "a")
file_h.setLevel("INFO")
formatter = logging.Formatter("%(asctime)s - %(module)s - %(message)s\n")
file_h.setFormatter(formatter)

# Print everything to the standard output
stdout_h = logging.StreamHandler()
stdout_h.setLevel("DEBUG")

# Name and generate logging object
logger = logging.Logger(__name__)
logger.addHandler(smtp_h)
logger.addHandler(file_h)
logger.addHandler(stdout_h)

# Format the logger output
FORMAT = "%(asctime)-15s %(message)s"
logging.basicConfig(format=FORMAT)
	

