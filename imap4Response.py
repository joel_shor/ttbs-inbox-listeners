# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Handles IMAP4 idle responses, starting the necessary function in response to a
push notification.
"""

import threading


def idle_response_handler(server_response, idle_check, start_f, args, thread_name):
    """Starts a function in response to an IMAP4 push notification."""
    if _ok_response(server_response) and _idle_response(idle_check) == None:
        threading.Thread(target=start_f, args=args, name = thread_name).start()
        
    elif _idle_response(idle_check) == 'UNKNOWN':
        logger.error('IMAP IDLE return type not recognized: %s'%(str(idle_check),))
        
    elif _idle_response(idle_check) == 'TIMEOUT':
        pass
    
def _ok_response(server_response):
    try:
        return server_response[0] == 'OK' and server_response[1][0] == 'IDLE terminated (Success)'
    except:
        return False

def _idle_response(idle_check):
    if idle_check == ('IDLE', ['TIMEOUT']):
        return 'TIMEOUT'
    if idle_check == ('IDLE', [None]):
        return None
    else:
        return 'UNKNOWN'
             
    