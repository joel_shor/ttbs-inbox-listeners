# Copyright (C) Joel Shor 2011, All rights reserved
# TigerTutoring Business Suite project

"""
An email interface for IMAP4 that allows for searching, getting, and push notification
"""

import smtplib
import imaplib2
import email
from email.mime.text import MIMEText


class IncorrectEmailHandlerArguments(Exception):
    """ If the handler __init__ is passed incorrect arguments. """
    pass

class FailedEmailRecipients(Exception):
    """ If there are SMTP emails that fail when sent. """
    pass

class EmailHandler(object):
    """ A class for handling automated email sending, email reading,
        and mailbox manipulations.
        
        User login information is stored
        when the object is initialized. Send email using SMTP, retreive
        emails using IMAP4. See "http://tools.ietf.org/html/rfc2060.html" for IMAP queries
        """

    def __init__(self, usr, host, **kwargs):
        """Initialize login credentials.
        
        There are two sets of initializers: (usr, host, pwd) or
        (usr, host, general parameters(pm), general passwords(pwds)). The
        trash bin can always be set as an optional keyword as "trash".
        host is either a string like "gmail.com" or a tuple
        of smtp/imap domain names."""
        self.usr = usr
        
        if isinstance(host,str):
            self.host = host
            self.smtpHost = "smtp."+self.host
            self.imapHost = "imap."+self.host
        elif len(host) == 2:
            self.smtpHost = host[0]
            self.imapHost = host[1]
        else:
            raise IncorrectEmailHandlerArguments
        
        try:
            self.pwd = kwargs["pwd"]
        except:
            try:
                self.pwd = kwargs["pwds"][usr]
            except:
                raise IncorrectEmailHandlerArguments
        
        try:
            self.trash = kwargs["trash"]
        except:
            try:
                self.trash = kwargs["pm"]["trash"]
            except:
                raise IncorrectEmailHandlerArguments
        
        self.smtpServer = None
        self.imapServer = None
        self.imapState = "Non-authenticated"
    
    def __del__(self):
        self.Logout()
    
    def __enter__(self):
        return self
  
    def __exit__(self, type, value, traceback):
        self.Logout()
        
    def Send(self, fromaddr, toaddr, subject="Subject", text=""):
        """ Send an email using smtp host information provided when
            object was initialized. """
            
        str = MIMEText(text)
        str["Subject"] = subject
        str["From"] = fromaddr
        str["To"] = ", ".join(toaddr)
        
        if self.smtpServer is None:     # If previous version was closed, or never created
            self.smtpServer = smtplib.SMTP(self.smtpHost)
            self.smtpServer.starttls()
            self.smtpServer.login(self.usr,self.pwd)
            
        failed_recipients = self.smtpServer.sendmail(fromaddr,toaddr,str.as_string())
        if len(failed_recipients) != 0:
            raise FailedEmailRecipients
            #raise Exception("Failed recipients:"+str(failed_recipients).strip("[]"))

    def Search(self, imap_search, mailbox="Inbox"):
        """ Returns a list of mail IDs matching the search criteria.
            
            A thin wrapper for the IMAP Search command """
        
        # Connecting to the gmail imap server
        if self.imapServer is None:
            self.imapServer = imaplib2.IMAP4_SSL(self.imapHost)
            self.imapServer.login(self.usr, self.pwd)
            self.imapState = "Authenticated"

        # Select mailbox
        self.imapServer.select(mailbox)
        self.imapState = "Selected"
        
        resp, items = self.imapServer.search(None, imap_search)
        
        # Get message IDs
        ids = items[0].split()
        
        return ids
    
    def Get(self, id_list):
        """ Returns a dictionary of dictionaries of the form
            {EmailID:{From,To,Subject,Date,Text}}
            
            imap_search is an IMAP query, syntax being found
            at "http://tools.ietf.org/html/rfc2060.html" """
        
        if self.imapState != "Selected":
            raise IMAPStateError
        
        searchResults = {}
        
        for i, id in enumerate(id_list):
            
            # Fetch the entire mail
            response, data = self.imapServer.fetch(id, "(RFC822)")
            
            email_body = data[0][1] # getting the mail content
            mail = email.message_from_string(email_body) # parsing the mail content to get a mail object
        
            # walk through parts to find payload entry
            for part in mail.walk():
                if part.get_content_type() != "text/plain":
                    continue
                #plaintext = part.get_payload().replace("=\r\n","\r\n") #strange "=" appear, so remove them
                plaintext = part.get_payload(decode=True)
                toAppend = {
                    "From": mail["From"],
                    "To":mail["To"],
                    "Subject": mail["Subject"],
                    "Date": mail["Date"],
                    "Text": plaintext
                }
                searchResults[id] = toAppend 
                break   # we have found the plaintext, so we are done with this item
            if len(searchResults) is not i+1:
                raise NoPlainTextFound
        return searchResults
    
    def Store(self, id, command, flag_list):
        """Does things like delete an email or make it "unseen".
        
        with EmailHandler.new(email, host, pm, pwds) as em:
            for id in em.Search("(SUBJECT "DUMMY TEST")"):
                em.Store(id, "-FLAGS", "\\SEEN")
        """
        if self.imapState != "Selected":
            raise ImapStateError
        
        self.imapServer.store(id, command, flag_list)

    def Delete(self, id_list, expunge=False):
        """ Move emails with IDs specified as argument into the trash folder.
            Gmail server takes care of everything once mail is copied to trash
            folder (no need to set original emails to delete, then expunge
            
            Eventually need to add expunge functionality """
        
        if self.imapState != "Selected":
            raise ImapStateError
        for id in id_list:
            type, data = self.imapServer.store(id, "+FLAGS", "\\Deleted")
            if type != "OK": raise Exception("Store failed.")
        if expunge:
            type, data = self.imapServer.expunge()
            if type != "OK": raise Exception("Expunge failed.")
        
    
    def Move(self, id_list, mailbox):
        if self.imapServer is None:
            raise ImapServerNotInitialized
        
        # Must be in seperate for loops - otherwise some emails don"t delete properly!
        for id in id_list:
            type, data = self.imapServer.copy(id, mailbox)
            if type != "OK":
                import pdb; pdb.set_trace()
                raise Exception("Copy failed.")
        for id in id_list:
            type, data = self.imapServer.store(id, "+FLAGS", "\\Deleted")
            if type != "OK": raise Exception("Store failed.")
        type, data = self.imapServer.expunge()
        if type != "OK": raise Exception("Expunge failed.")
        
    
    def Idle(self, mailbox = None, timeout = None):
        
        if self.imapState == "Non-authenticated":
            self.imapServer = imaplib2.IMAP4_SSL(self.imapHost)
            self.imapServer.login(self.usr, self.pwd)
            self.imapState = "Authenticated"
        if mailbox is not None:
            self.imapServer.select(mailbox)
            self.imapState = "Selected"
        elif self.imapState != "Selected":
            raise ImapStateError
        self.imapServer.select(mailbox)
        self.imapState = "Selected"

        return self.imapServer.idle(timeout = timeout)
        
    def Logout(self):
        """ Close and logout of smtp and imap connections associated with
            this object """
            
        if self.smtpServer is not None:
            self.smtpServer.quit()
            self.smtpServer = None
        if self.imapServer is not None:
            self.imapServer.logout()
            self.imapServer = None