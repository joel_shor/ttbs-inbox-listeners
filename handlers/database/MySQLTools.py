# Copyright (C) Joel Shor 2011, All rights reserved
# TigerTutoring Business Suite project

"""
A wrapper class for standardizing calls to the database so that
code looks cleaner, and so that the databases can be interchangeable.
"""

import MySQLdb
 

# Base exception class
class MySQLError(Exception):
  def __init__(self, str=None):
    self.str = str

class InvalidParameters(MySQLError):
  pass

class InvalidArgumentsToFormat(MySQLError):
  def __str__(self):
    print "Arguments:",self.str

class DatabaseConnectionError(MySQLError):
  pass

class InsertionError(MySQLError):
  pass

class PlaceError(MySQLError):
  pass

class MySQLHandler(object):
  def __init__(self, *args):
    """Opens a cursor to a mysql database."""
    if len(args) == 2:
      host = args[0]["host"]
      user = args[0]["user"]
      password = args[1][user]
      db = args[0]["db_name"]
      port = args[0]["port"] if "port" in args[0] else 3306
    elif len(args) == 4 or len(args) == 5:
      host = args[0]
      user = args[1]
      password = args[2]
      db = args[3]
      port = args[4] if len(args) == 5 else 3306
    else:
      raise InvalidParameters
    try:
      self.mysql_client = MySQLdb.connect(host=host,user=user,passwd=password,db=db,port=port)
    except MySQLdb.Error:
      raise DatabaseConnectionError
    self.c = self.mysql_client.cursor()
  
  def __del__(self):
    self.Close()
    
  def __enter__(self):
    return self
  
  def __exit__(self, type, value, traceback):
    self.Close()
  
  def Close(self):
    self.c.close()
    self.mysql_client.commit()
  
  def Select(self, cols, tables, conditions=None, orderC=None, order='ASC'):
    """ Select rows of a mySQL table.
    
        'cols', 'tables', are each either a single
          string, the empty string for default behavior, or a list of
          multiple strings
        'conditions' is a dictionary
        'orderC' is either None, or a row name
        'order' is either 'ASC' or 'DESC')
        
        Output is a tuple of rows returned by the mySQL query.
        Each row of data is a tuple of data, with the ith element
        corresponding to the ith column requested """
        
    try:
      col_str, tb_str, whr_str, val_tup = self._FormatSelect(cols,tables,conditions, orderC, order)
    except InvalidArgumentsToFormat, e:
      error_str = "%s in SELECT(%s, %s, %s): %s"% (str(e.str), str(cols),str(tables),str(conditions), str(orderC))
      raise InvalidArgumentsToFormat(error_str)
    query = "SELECT " + col_str + " FROM " + tb_str + whr_str
    self.c.execute(query, val_tup)
    return self.c.fetchall()
  
  def Insert(self, table, cols, values):
    """ Insert values into a mySQL table.
    
        'table' is a single string
        'cols' is either a string or tuple
        'values' is either a string, a tuple of values, or a list of tuples,
        where each tuple is one row to be inserted """
        
    if values is None or values == []:
      return
    try:
      tb_str, col_str, val_str, val_tup = self._FormatInsert(table,cols,values)
    except InvalidArgumentsToFormat, e:
      error_str = "%s in INSERT(%s, %s, %s)" % (str(e.str), str(table), str(cols), str(values))
      raise InvalidArgumentsToFormat(error_str)
    query = "INSERT INTO "+tb_str+col_str+" VALUES "+val_str
    try:
      return self.c.executemany(query, val_tup)
    except Exception as e:
      raise InsertionError

  def Place(self, table, values, return_col = None):
    """ Insert values into a table if not already there, otherwise do nothing.
        
        table is a string
        values is a dictionary with key being the column
    
        Should be improved to check / insert multiple values in multiple
        rows """
        
    entries = self.Select(return_col, table, values)
    action = 'Nothing'
    if len(entries) == 0:
      test = self.Insert(table, values.keys(), values.values() )
      if return_col: entries = self.Select(return_col, table, values)
      action = 'Insert'
    if return_col:
      try:
        if entries == ():
          raise Exception('Values failure.')
        return action, entries[0][:len(return_col)]
      except:
        raise PlaceError
    else:
      return action
  
  def Override(self, table, checking_c, values, order_c, order_description='DESC'):
    """ Insert values into  a table if not already there, otherwise update the first row
        in the result.
        
        Returns 'Insert' if inserted, 'Update' if updated
        
        'table' is a string
        'checking_c' is a string or a tuple of column names to match
          if data should be inserted or updated
        'values' is a dictionary with key being the column
        'order_c' is the order column
        'order_description' is how results should be ordered """
    try:   
      if isinstance(checking_c,str):
        checking_dict = {checking_c: values[checking_c]}
      else:
        checking_dict = dict((k,values[k]) for k in checking_c if k in values)
    except Exception:
      raise InvalidArgumentsToFormat(checking_c)
    prev_entries = self.Select(values.keys(), table, checking_dict, order_c, order_description)
    if len(prev_entries) > 0: # IF ENTRY EXISTS
      self.Update(table, values, dict( zip(values.keys(), prev_entries[0]) ) )
      return 'Update'
    else: # IF ENTRY DOES NOT EXIST
      self.Insert(table, values.keys(), values.values() )
      return 'Insert'
  
  def Update(self, table, values, conditions):
    """ Update a row in a mySQL table.
      
        'table' is a string
        'values' is a dictionary with key being the column
        'conditions' is a string or a dictionary """
    tb_str, set_str, cond_str = self._FormatUpdate(table, values, conditions)
    query = "UPDATE " + tb_str + " SET " + set_str + " WHERE " + cond_str
    val_tup = tuple(values.values()) + tuple(conditions.values())

    self.c.execute(query, val_tup)
  
  def _FormatSelect(self, cols, tables, conditions, orderC, order):
    # Columns
    if cols == [] or cols == '' or cols == None:
      col_str = '*'
    elif isinstance(cols,str):
      col_str = cols
    elif isinstance(cols,list):
      col_str = ', '.join(cols)
    else:
      raise InvalidArgumentsToFormat('Columns')
    # Tables
    if isinstance(tables,str):
      tb_str = tables
    elif isinstance(tables,list):
      tb_str = ', '.join(tables)
    else:
      raise InvalidArgumentsToFormat('Tables')
    # Conditions
    if conditions is None:
      whr_str = ''; val_tup = ()
    elif isinstance(conditions, dict):
      whr_str = ' WHERE ' + "<=>%s AND ".join(conditions.keys()) + "<=>%s"
      val_tup = tuple(conditions.values())
    else:
      raise InvalidArgumentsToFormat('Conditions')
    # Order
    if orderC is None:
      pass
    elif isinstance(orderC, str) and (order == 'ASC' or order == 'DESC'):
      whr_str = whr_str + ' ORDER BY ' + orderC + ' ' + order
    else:
      raise InvalidArgumentsToFormat('Order')
    return (col_str, tb_str, whr_str, val_tup)
    
  def _FormatInsert(self, table, cols, vals):
    # Tables
    if isinstance(table, str):
      tb_str = table
    else:
      raise InvalidArgumentsToFormat('Table')
      
    # Cols
    if cols is not '' and isinstance(cols,str):
      col_str = '('+ cols + ')'
    elif cols is not [] and isinstance(cols,list):
      col_str = ' ('+', '.join(cols)+')'
    else:
      raise InvalidArgumentsToFormat('Columns')
    
    #Value string
    if isinstance(cols, list) or isinstance(cols, tuple):
      val_str = '(%s'+', %s'*(len(cols)-1) +')'
    else:
      val_str = '(%s)'
    
    # Value tuple
    if isinstance(vals, list) and vals[0] is not None and isinstance(vals[0],tuple):
      val_tup = vals
    elif isinstance(vals, list):
      val_tup = [tuple(vals)]
    elif isinstance(vals, tuple):
      val_tup = [vals]
    else:
      val_tup = [(vals,)]
    return (tb_str, col_str, val_str, val_tup)
  
  def _FormatUpdate(self, table, values, conditions):
    # Tables
    if not isinstance(table,str):
      raise InvalidArgumentsToFormat('Tables: '+str(table))
    tb_str = table
    
    # Cols and values
    try:
        set_str = ' = %s, '.join(values.keys()) + ' = %s'
    except Exception:
      raise InvalidArgumentsToFormat('Update values: '+str(values))
    
    # Conditions
    try:
      cond_str = ' <=> %s AND '.join(conditions.keys()) + ' <=> %s'
    except Exception:
      raise InvalidArgumentsToFormat('Update conditions: '+str(conditions))
    return (tb_str, set_str, cond_str)

  