# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
A placeholder class for a spreadsheet handler.
"""


class DummySpdshtHandler(object):
    """A placeholder class for a spreadsheet handler."""
    
    def __init__(self, *args, **kwargs):
        pass
    
    def __enter__(self):
        return self
  
    def __exit__(self, type, value, traceback):
        pass