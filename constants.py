# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
File for various categories of program parameters.
"""

from passwords import pwds

email_pm = {
        "main": "tigertutoringservice",
        "auto": "automatednotification95",
        "host": "gmail.com",
        "trash": "[Gmail]/All Mail",
        "inbox": "Inbox"}

exc_pm = {
        "folder": "logs",
        "from": "automatednotification95",
        "to": "tigertutoringservice@gmail.com",
        "smtp": ("smtp.gmail.com",587)}

th_pm = {
        "reg_name": "TTBS",
        "safety_name": "Safety",
        "start_name": "Startup"}

db_pm = {
        "host": "69.255.44.15",
        "port": 10500,
        "user": "root",
        "db_name": "tigertutoring"}

spdsht_pm = {
        "owner": "tigertutoringservice@gmail.com"
}
