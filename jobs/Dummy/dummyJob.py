# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Job placeholder for debugging purposes.
"""

from constants import email_pm


name = "Dummy job"
resources = {'Email': email_pm["auto"],
             'Email_host': email_pm["host"],
             'Imap_query': '(UNSEEN FROM "shor.joel@gmail.com" SUBJECT "DUMMY TEST")'}

def initialize_job():
	return

def process_emails(email_h, db_h, spdsht_h, email_dict, logger):
	return