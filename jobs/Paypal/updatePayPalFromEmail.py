# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Main functions to transfer PayPal email information into a database.
"""

import re
import warnings

from constants import email_pm, db_pm, spdsht_pm, pwds
from handlers.database.MySQLTools import MySQLHandler as db_h_class
from paypalLib import check_type, parse_receive, parse_receipt, old_parse_receive


name = "Update PayPal Db from Email"
resources = {'Email': email_pm["auto"],
             'Email_host': email_pm["host"],
             'Imap_query': r'(UNSEEN FROM "%s")' % ('service@paypal.com')}
tb_name = 'payments'
init_script = """create table if not exists %s(
Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
Direction enum('Received','Sent'),
Type enum('PayPal', 'Thumbtack'),
Email varchar(100),
Amount decimal(5,2),
Description text,
ClientName varchar(100),
InvoiceID int(11)
) ENGINE = InnoDB;
"""%(tb_name)

def initialize_job():
	"""Initialize table to store PayPal receipts."""
	with db_h_class(db_pm, pwds) as db_h:
		with warnings.catch_warnings():
			warnings.simplefilter("ignore")
			db_h.c.execute(init_script)

def process_emails(email_h, db_h, spdsht_h, email_dict, logger):
	"""Process an email when push notification is sent."""
	for email_id, email in email_dict.items():
		type = check_type(email)
		if type == "receive_invoice" or type == "receive_payment":
			data = parse_receive(email)
			db_h.Insert(tb_name, data.keys(), data.values())
			logger.error("PayPal Parsed Invoice %s: %s from %s", data["InvoiceID"], data["Amount"], data["ClientName"])
		elif type == "old_receive_invoice":
			data = old_parse_receive(email)
			db_h.Insert(tb_name, data.keys(), data.values())
			logger.error("PayPal Parsed Old Invoice %s: %s from %s", data["InvoiceID"], data["Amount"], data["ClientName"])
		elif type == "receipt":
			data = _parse_receipt(email)
			db_h.Insert(tb_name, data.keys(), data.values())
			logger.error("PayPal Parsed Receipt: %s from %s", data["Amount"], data["ClientName"])