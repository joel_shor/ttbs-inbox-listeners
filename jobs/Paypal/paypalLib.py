# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Module to support process_inbox and initialize_job.
"""

import re


def check_type(email):
	""" Return a tuple of type of email and corresponding job status number - 3 means
		invoice sent, 4 means invoice is paid. """
		# 1 is scheduled, 2 is completed, 3 is paypal sent, 4 is paypal paid
	if re.search('You sent an invoice',email['Subject']): 
		return ('sent_invoice')
	# This is a Subject line PayPal used years ago
	elif re.search('has just paid for your', email['Subject']): 
		return ('old_receive_invoice')
	elif re.search('just paid for your invoice', email['Subject']): 
		return ('receive_invoice')
	elif re.search('just sent you a payment', email['Subject']): 
		return ('receive_payment')
	elif re.search('Receipt for Your Payment', email['Subject']):
		return 'receipt'
	else: return -1

def old_parse_receive(msg):
	"""Convert PayPal's automating email into more meaningful information."""
	name, email = re.search('Customer\r\n(.*?)\r\n(.*?)\r\n', msg['Text']).groups()
	amount = re.search('Payment[: \t=]*\$([0-9.]*)[ \t]*?USD', msg['Text']).group(1)
	description = re.search('\r\n(Details.*?)Invoice ID', msg['Text'], flags=re.DOTALL).group(1)
	invoiceid = re.search('has just paid for your invoice ([0-9]+)', msg['Subject']).group(1)
	subj_and_description = msg['Subject'] + '\n' + description
	return {'ClientName': name, 'Email': email, 'Amount': amount, 'Direction': 'Received',
		'Type': 'PayPal', 'Description': subj_and_description, 'InvoiceID': invoiceid}

def parse_receive(msg):
	"""The same as parse_old_receive except that 'has just paid' is changed to 'just paid'"""
	name, email = re.search('Customer\r\n(.*?)\r\n(.*?)\r\n', msg['Text']).groups()
	amount = re.search('Payment[: \t=]*\$([0-9.]*)[ \t]*?USD', msg['Text']).group(1)
	description = re.search('\r\n(Details.*?)Invoice ID', msg['Text'], flags=re.DOTALL).group(1)
	invoiceid = re.search('just paid for your invoice ([0-9]+)', msg['Subject']).group(1)
	subj_and_description = msg['Subject'] + '\n' + description
	return {'ClientName': name, 'Email': email, 'Amount': amount, 'Direction': 'Received',
		'Type': 'PayPal', 'Description': subj_and_description, 'InvoiceID': invoiceid}


def parse_receipt(msg):
	"""Convert PayPal's automating email into more meaningful information."""
	name = re.search('Merchant:\r\n(.*?)\r\n', msg['Text']).group(1)
	description = re.search('Description:(.*?)\r\n', msg['Text']).group(1)
	amount = re.search('Payment: \$(.*?) USD\r\n', msg['Text']).group(1)
	email = re.search('Payment sent to: (.*?)\r\n', msg['Text']).group(1)
	subj_and_description = msg['Subject'] + '\n' + description
	return {'ClientName': name, 'Email': email, 'Amount': amount, 'Direction': 'Sent',
		'Type': 'PayPal', 'Description': subj_and_description, 'InvoiceID': None}
        