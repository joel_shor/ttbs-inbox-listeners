# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Takes a list of modules that all are listening to the same inbox. Since the push
notification doesn"t say what kind of email was received, process the inbox on
all modules.
"""

import threading

from customLogging import logger
from constants import th_pm, email_pm, db_pm, pwds
from handlers.email.EmailTools import EmailHandler as EmailClass
from handlers.database.MySQLTools import MySQLHandler as DBClass
from handlers.spreadsheet.DummySpdshtTools import DummySpdshtHandler as SpdshtClass


def process_inbox(module_list, active_l):
    """ Run the modules in module_list on the same mailbox.
    
        Once the mailbox server signals a daemon via IMAP"s IDLE command
        that mail is received, run this function. It begins a new thread
        of execution. """
    try:
        active_l.acquire(True)
        
        logger.debug("Processing inboxes...")
        email, email_host, spdsht_account = _get_resources(module_list[0].resources)
        with    EmailClass(email, email_host, pm=email_pm, pwds=pwds) as email_h, \
                DBClass(db_pm, pwds) as db_h, \
                SpdshtClass(spdsht_account, pwds) as spdsht_h:
            for mod in module_list:
                ids = email_h.Search(mod.resources["Imap_query"], email_pm["inbox"])
                email_dict = email_h.Get(ids)

                _update_status_msg(threading.current_thread().name,
                                   email_dict, mod.name)
        
                if not email_dict:
                    continue
                
                logger.debug("%s takes %i emails", mod.name, len(email_dict))
                try:
                    mod.process_emails( email_h=email_h,
                                        db_h=db_h,
                                        spdsht_h=spdsht_h,
                                        email_dict=email_dict,
                                        logger=logger)
                except:
                    logger.exception("%s process emails failed", mod.name)
                logger.debug("Done with %s", mod.name)
    except:
        logger.exception("Resource handlers failed.")
        raise
        
    finally:
        try:
            active_l.release() # Might run in to trouble if thread doesn"t have active lock
        except Exception:
            pass
        
def _update_status_msg(name_th, email_dict, mod_prog_n):
    if name_th is th_pm["start_name"] and email_dict:
        logger.info("Safety run picked up %i emails for %s", len(email_dict), mod_prog_n)
    
    if name_th is th_pm["reg_name"] and email_dict:
        logger.info("Regular run picked up %i emails for %s", len(email_dict), mod_prog_n)

def _get_resources(resource):
    email = resource['Email']
    email_host = resource['Email_host']
    try:
        spdsht_account = resource['Spdsht_account']
    except:
        spdsht_account = None
    return email, email_host, spdsht_account
    