# Copyright (C) Joel Shor 2013, All rights reserved
# TigerTutoring Business Suite project

"""
Parse the command line options.
"""

from customLogging import logger


def parse_cmd_options(argv):
    """Parse the command line options."""
    if len(argv) < 3:
        raise Exception("Number of arguments not right.")
    log_lvls = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
    if argv[-1] not in log_lvls:
        raise Exception("Last argument must be in %s"%(str(log_lvls),))
    log_lvl = argv[-1]
    
    module_list = []
    if "paypal" in argv:
        from jobs.Paypal import updatePayPalFromEmail
        module_list.append(updatePayPalFromEmail)
    if "tutors" in argv:
        raise Exception("'%s' not supported in this version."%("tutors"))
    if "requests" in argv:
        raise Exception("'%s' not supported in this version."%("requests"))
    if "feedback" in argv:
        raise Exception("'%s' not supported in this version."%("feedback"))
    if "completion" in argv:
        raise Exception("'%s' not supported in this version."%("completion"))
    if "jobs" in argv:
        raise Exception("'%s' not supported in this version."%("jobs"))
    if "thumbtack" in argv:
        raise Exception("'%s' not supported in this version."%("thumbtack"))
    if "dummy" in argv:
        from jobs.Dummy import dummyJob
        module_list.append(dummyJob)
    if module_list == []:
        raise Exception("No modules recognized.")
        
    return module_list, log_lvl
    